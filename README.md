# Git Training

A testbed for demonstrating git

## Pre-requisites

Figure out how to install git first.

 - If you're using Windows, maybe try 'git for Windows'. But isn't the syntax for the git console you download basically Linux anyway? (Running minGW underneath?)

 - If you're using Windows but like Linux, maybe try 'Cygwin' or 'Ubuntu Bash for Windows 10', and then install the git package (for its respective method).

 - If you're using Linux, `sudo apt-get` it.

Is it easier to create repositories with the GUI / web interface than in command line?

Anyway, if the repository already exists, first thing you want to do is clone it. Unless you have a ssh key already set up between the computer you're currently using and your git/bitbucket account, you need to use the HTTPS link as per below: 

`git clone https://icecreambean@bitbucket.org/icecreambean/git-training.git`

---
## Adding, modifying and uploading a file to git

`git status`

`git add file1 file2 "file 3"`  <-- register files for commit

`git add .`  <-- register all files in current dir

`git commit -m "write a msg to let others know what you changed"`  <-- register changes made to files

`git push`  <-- upload file's changes to repository

`git config --global user.name "myUserName"`  <-- if it's your first time, register what account you're going to be telling git to use

Regarding switching between git accounts: 

https://stackoverflow.com/questions/21615431/git-pushes-with-wrong-user-from-terminal

---
## Downloading other people's changes from the repository to your local folder

`git pull`  <-- "dangerous" combination of `git fetch` and `git merge` 

If there have been any updates (from other people) to the branch you yourself are trying to update to, you will be rejected until you are up-to-date on the current branch you are on.

If there are changes that you need to save across this process, you may want to do this first:

`git diff`  <-- git tracks changes by comparing the difference between the current and original file in the .git folder

`git stash`  <-- store local changes, if you about to pull or branch (as they haven't been uploaded to the repository yet)

`git stash list`  <-- lists all the times you've stored changes (that you haven't released)

`git pull`

`git stash apply`  <-- re-apply the changes

https://git-scm.com/book/en/v1/Git-Tools-Stashing

---
## Working in a separate workspace (branch) from everyone else

`git branch`  <-- list existing branches

`git branch "BranchName"`  <-- clones the current branch you're in and creates a new branch of "BranchName"

`git checkout "BranchName"`  <-- switch to new branch (workspace)

A branch is some version of your current body of work  (let's call this OriginalBranch). When you create a new branch (let's call this NewBranch), you are copying the contents of that body of work. This means NewBranch looks identical to OriginalBranch, as it has copied all its files (or, more accurately, copied all its commits or changes since the very start of the repository). If you then decide to check-out and work in NewBranch instead, changes made to NewBranch do not change OriginalBranch. This allows you to do some experimental work in NewBranch without affecting or damaging the working code already in OriginalBranch that you don't want to break.

(Make your file and directory changes within the new branch)

`git add .`

`git commit -m "some message"`

`git push --set-upstream origin "BranchName"`  <-- some legacy-compatible thing you have to do for new branches?

`git push`

https://stackoverflow.com/questions/37770467/why-do-i-have-to-git-push-set-upstream-origin-branch

https://stackoverflow.com/questions/9834689/comparing-two-branches-in-git

---
## Merging your changes back with the original code
`git merge "BranchName"`  <-- combine a different branch "BranchName" into the local version of whatever current branch you're in

If there are conflicts, compare changes. Git will modify each file that has conflicts registered by marking outlining the block of text that is conflicting with another block of text (written in from the file we were trying to merge from the other branch).

After all conflicts have been resolved, do the following:

`git add .`

`git commit -m "my merge comment"`

`git push`  <-- push the local copy of the merged "BranchName" contents to the repository, for your current branch.

The process of merging is as follows:

In your current branch (e.g. master (the 'root' of all branches)), you want to pull the contents of another branch (e.g. BranchA) into it. This means, after the initial `git merge` the contents within the files of BranchA have been combined, as best they can, with the contents of the branch 'master'. If the contents are identical, there is no duplication of these contents. If the contents are not identical, then a conflict is raised and git modifies that file to show that there is a choice of either applying BranchA's file content or 'master's file content.

If you are happy with how BranchA's code has been combined into 'master's code, then you can `git push` to upload these changes to the repository.

Merging the branch does not delete the branch. Does that mean you can continue to work on that branch (that you just merged) and later re-merge it again?

https://stackoverflow.com/questions/35738790/how-to-close-a-branch-in-git

The above link suggests to archive and then delete the branch.

---
## Merging vs rebasing
Merging is used to close off a branch (e.g. BranchA) that you are happy you have finished developing in. Note that 'master' is the original path in your code. Assuming all branches complete their work, you want to eventually merge all of them back to 'master', which represents your final product.

If you `git branch` off another branch (e.g. master) into BranchA, and 'master' goes through a whole suite of updates (including bug fixes) that BranchA now wants access to, you want to update where BranchA split off from 'master' to the head (or current) position of 'master'.

The command for this is: `git rebase master`  <-- (or replace 'master' with any other branch you want)

https://medium.com/datadriveninvestor/git-rebase-vs-merge-cc5199edd77c

---
## Reverting changes ('undoing' commits)

https://git-scm.com/book/en/v2/Git-Basics-Viewing-the-Commit-History

`git log`  <-- display all previous records of commits done

Commits cannot be undone, only reverted. Reverting a commit doesn't remove the old commit, it just undoes the changes that commit applied. The old commit is still a record in your commit log.

Reverting commits need to be done in sequence, one at a time. If you just selectively revert on one commit in the middle of a series of commits you want to revert, it will only revert that one commit (as opposed to the patch of commits you were intending). In other words, you need to specify the range of commits (e.g. using the listed alphanumeric ID for that commit) to remove using the `A..B` notation, where 'A' is the commit you want to keep (the earlier commit) and 'B' is the current commit.

https://stackoverflow.com/questions/1463340/how-to-revert-multiple-git-commits

`git revert A..B`  <-- revert from the "current" commit B back to the commit you want (A)

---
## ...And More?

There's probably a lot more to git. I don't really know it that well. Use Google and StackOverflow...

---
## Using ssh keys instead of typing your password every time

This should cover it: https://help.github.com/en/articles/connecting-to-github-with-ssh

Note: if you choose to have a 'passphrase' (which encrypts your private SSH key), you will have to type out your passphrase every single time. If you don't choose to have a passphrase (by hitting Enter when the command prompts asks you to type one in (optional)), then you can `git push` without needing to type in any credentials.

First, create SSH key for your computer:

 * This link should cover the details: https://help.github.com/en/articles/generating-a-new-ssh-key-and-adding-it-to-the-ssh-agent

Then, on Bitbucket and/or GitHub, etc.:

 * Go to your profile settings

 * Go to 'SSH keys' under 'Security'. You can add the public SSH key you generated there.

 * For security purposes (for encryption to work), don't share your unencrypted private SSH key with anyone. That is, if you care about the security of any of your code repositories.

If your current repository is accessed via HTTPS, you also need to switch over to SSH. This link should cover it: https://help.github.com/en/articles/changing-a-remotes-url

For this repository, we have:

 * HTTPS: https://icecreambean@bitbucket.org/icecreambean/git-training.git

 * SSH: git@bitbucket.org:icecreambean/git-training.git

